export interface Navigation
{
  id: number;
  url: string;
  name: string;
  name_en: string;
}
