export interface Schedule {
  id: number;
  date: string;
  date_string: string;
  date_string_en: string;
  date_string_large: string;
  date_string_large_en: string;
  events: [Event];
}

export interface Event {
  title: string;
  title_en: string;
  description: string;
  type: string;
  date: string;
  hour: string;
  speakers: [Speaker];
  moderators: [Moderator];
}

export interface Speaker {
  id: number;
  name: string;
  photo: string;
}

export interface Moderator {
  id: number;
  name: string;
  photo: string;
}
