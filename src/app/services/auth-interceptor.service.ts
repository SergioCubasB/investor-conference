import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor{

  constructor() { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE2NTkyMDUwMDcsImlzcyI6ImN1Ym9yb2pvIiwibmJmIjoxNjU5MjA1MDA3LCJleHAiOjE2NjQzODkwMDcsImRhdGEiOnsiaWQiOjEsInVzZXJuYW1lIjoiY3ViaXRvQGN1Ym9yb2pvLnBlIn19.2TTXWpc21wnJdFr6wI32lSVOpwOBrznofZzroWvTyNj2lE6juXwAy1210D6_NlI60b8mZcQ8MXfkqoTEGQ9dBQ"; //localStorage.getItem("_TK");
    
    let jwtoken = req.clone({
      setHeaders:{
        Authorization: `Bearer ${token}`
      }
    })

    return next.handle(jwtoken);

  }
}
