import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  getNavigations() {
    return this.httpClient.get('assets/data/navigation.json');
  }

  getConfig() {
    return this.httpClient.get('assets/data/config.json');
  }

  getCredicorp() {
    return this.httpClient.get('assets/data/credicorp.json');
  }

  getFooter() {
    return this.httpClient.get('assets/data/footer.json');
  }

}
