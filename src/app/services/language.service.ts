import { EventEmitter, Injectable, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  @Output() language: EventEmitter<string>;

  constructor(
    private httpClient: HttpClient,
  ) {
    this.language = new EventEmitter();
  }

  traslate(lang) {
    sessionStorage.setItem('_LANG', lang);
    this.language.emit(lang);
  }
}
