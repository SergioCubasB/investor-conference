import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private env = environment;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(
    private httpClient: HttpClient,
  ) { }

  postRegiste(formData) {
    return this.httpClient.post(`${this.env.api}/user`, formData);
  }

  getUser() {
    return this.httpClient.get(`${this.env.api}/user`);
  }

  getListSpectator() {
    return this.httpClient.get(`${this.env.api}/event/list-viewers`);
  }

  getNoRegister(param) {
    const lang = param.lang ? param.lang : 'es';
    return this.httpClient.get(`${this.env.api}/email/asistencia-cancelada?email=${param.email}&lang=${lang === 'es' ? '1' : '2'}`);
  }

  postValidateToken(token) {
    const formData = new FormData();

    formData.set('token', token);

    return this.httpClient.post(`${this.env.api}/user/validate-token`, formData);
  }

  postSpectator(formData) {
    return this.httpClient.post(`${this.env.api}/event/view`, formData);
  }

  putSpectator(id, data) {
    const params = new HttpParams({
      fromObject: data,
    });
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };
    return this.httpClient.put(`${this.env.api}/event/view/${id}`, params.toString(), httpOptions);
  }

  downloadUsers() {
    return this.httpClient.get(`${this.env.api}/user/download`, { responseType: "blob" });
  }

  downloadSpectator() {
    return this.httpClient.get(`${this.env.api}/event/list-viewers/download`, { responseType: "blob" });
  }

}
