import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  private env = environment;
  protected api: string;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(
    private httpClient: HttpClient,
  ) { 
    this.api = `${environment.api}`;
  }

  getSchedule(token) {
    return this.httpClient.get(`${this.api}/event/complete?token=${token}`);
    // return this.httpClient.get('assets/data/schedule.json');
  }

  
  getSliders() {
    return this.httpClient.get(this.api + '/homeheader');
  }

  getMySchedule(token) {
    return this.httpClient.get(`${this.api}/event/user?token=${token}`);
    // return this.httpClient.get('assets/data/myagenda.json');
  }

  postEvents(formData) {
    return this.httpClient.post(`${this.api}/user/save-event`, formData);
  }

  getSpeaker(idSpeaker) {
    return this.httpClient.get(`${this.api}/participant/${idSpeaker}?expand=EventsbySpeaker,file `);
  }

  getModerator(idModerator) {
    return this.httpClient.get(`${this.api}/participant/${idModerator}?expand=EventsbyModerator,file `);
  }

  getEvent(idEvent) {
    return this.httpClient.get(`${this.api}/event/${idEvent}?expand=details,speakerComplete,moderatorComplete`);
  }

  getEvents() {
    return this.httpClient.get(`${this.api}/event?expand=details,speakerComplete,moderatorComplete`);
  }

  getCalendar(calendar: string) {
    return this.httpClient.get(`${this.api}/calendar/${calendar}`);
  }

  postCalendar(calendar: string, formData) {
    return this.httpClient.post(`${this.api}/calendar/${calendar}-save`, formData);
  }

  posCaledarOutlook(code) {
    return this.httpClient.get(`${this.api}/oauth/microsoft?code=${code}`);
  }

  getSpeakerModerator(id: number) {
    return this.httpClient.get(`${this.api}/participant?type=${id}&expand=file`);
  }

  getLiveVimeo(live_event_id) {
    return this.httpClient.get(`https://vimeo.com/api/oembed.json?url=https%3A%2F%2Fvimeo.com%2Fevent%2F${live_event_id}`);
  }

  getLiveYoutube(channel_id) {
    return this.httpClient.get(`https://www.youtube.com/embed/live_stream?channel=${channel_id}`);
  }

  postQuestions(formData) {
    return this.httpClient.post(`${this.api}/user/question`, formData);
  }

}
