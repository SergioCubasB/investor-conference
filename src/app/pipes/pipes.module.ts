import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DomseguroPipe } from './domseguro.pipe';
import { DateZonePipe } from './date-zone.pipe';



@NgModule({
  declarations: [
    DomseguroPipe,
    DateZonePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DomseguroPipe,
    DateZonePipe
  ]
})
export class PipesModule { }
