import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment-timezone';

@Pipe({
  name: 'dateZone'
})
export class DateZonePipe implements PipeTransform {

  transform(date: string, zone: string = 'America/Lima', format: string): string {
    // console.log('transform :>> ', date, zone, moment(`${date} -05:00`).tz(zone).format(format));
    return moment(`${date} -05:00`).tz(zone).format(format);
  }

}
