import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { RegisterComponent } from './landing/register/register.component';
import { SharedModule } from './shared/components/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './shared/material/material.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { OauthComponent } from './landing/oauth/oauth.component';
import { NoregisterComponent } from './landing/noregister/noregister.component';
import { EventLiveComponent } from './landing/event-live/event-live.component';
import { PrivacyPolicyComponent } from './landing/privacy-policy/privacy-policy.component';
import { ServiceConditionsComponent } from './landing/service-conditions/service-conditions.component';
import { MomentModule } from 'ngx-moment';
import { PipesModule } from './pipes/pipes.module';
import { AuthInterceptorService } from './services/auth-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    OauthComponent,
    NoregisterComponent,
    EventLiveComponent,
    PrivacyPolicyComponent,
    ServiceConditionsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    MomentModule,
    PipesModule,
  ],
  providers: [
    /*{
      provide: HTTP_INTERCEPTORS, 
      useClass: AuthInterceptorService, 
      multi:true
    }*/
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
