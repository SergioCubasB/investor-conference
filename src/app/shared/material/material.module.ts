import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';

const material = [
  MatDialogModule,
  MatIconModule
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    material,
  ],
  exports: [
    material
  ]
})
export class MaterialModule { }
