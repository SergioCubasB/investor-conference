import { Component, Input, OnInit } from '@angular/core';
import { Footer } from '../../../interfaces/footer';
import { LanguageService } from '../../../services/language.service';
import { SharedService } from '../../../services/shared.service';
import { FooterService } from './services/footer.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  lang: string = 'es';
  footer: any;
  @Input() backgroundColor: boolean = false;

  data_es = [];
  data_en = [];
  data_social =[];
  logoFooter = "";
  logoFooterResponsive = "";


  constructor(
    private serviceLanguage: LanguageService,
    private serviceShared: SharedService,

    private footerService: FooterService
  ) { }

  ngOnInit(): void {
    this.lang = sessionStorage.getItem('_LANG');
    this.getFooter();
    this.switchLang();
  }

  switchLang() {
    this.serviceLanguage.language.subscribe(
      (resp) => {

        this.lang = resp;
      }
    );

    this.data_en = [];
        
    this.footerService.getFooter()
    .subscribe(
      (response:any) => {
        const  data  = response;
        this.data_es = [];
        this.data_en = [];

        data.forEach( (element, index) => {

          this.data_es.push({
            offices: element.name,
            link: element.url
          })

          this.data_en.push({
            offices: element.name_en,
            link: element.url
          })

        })

      }
    )

    this.footerService.getSocial()
    .subscribe(
      (response:any) => {
        const  data  = response;
        this.data_social = [];

        data.forEach( (element, index) => {
          this.data_social.push(element)
        })

      }
    )


    this.footerService.getLogo()
    .subscribe(
      (data:any) => {
        const { file } = data[0];
        this.logoFooter = 'http://159.223.201.87/'+file.route;
        this.logoFooterResponsive = 'http://159.223.201.87/'+file.route_footer;
      }
    )
  }

  getFooter() {
    this.serviceShared.getFooter().subscribe(
      (resp: Footer) => {
        this.footer = resp;
      }
    );
  }

  gotoPage(page: string) {
    window.open(page, "_blank");
  }

}
