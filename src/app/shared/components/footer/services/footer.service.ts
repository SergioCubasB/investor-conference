import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})
export class FooterService {
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${environment.api}`;
    }

    getFooter(){
        return this.http.get(this.api + '/homefooter');
    }

    getSocial(){
        return this.http.get(this.api + '/social-network');
    }

    getLogo(){
        return this.http.get(this.api + '/logo');
    }
}