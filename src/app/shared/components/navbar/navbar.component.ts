import { Component, OnInit } from '@angular/core';
import { LanguageService } from '../../../services/language.service';
import { SharedService } from '../../../services/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  lang: string;
  config_navbar: any;
  user: any;

  constructor(
    private serviceLanguage: LanguageService,
    private serviceConfig: SharedService,
    private navigate: Router,
  ) {
    this.user = localStorage.getItem('USER-DATA') ? JSON.parse(localStorage.getItem('USER-DATA')) : '';
  }

  ngOnInit(): void {
    this.switchLang('es');
    this.getConfig();
  }

  switchLang(lang) {
    this.lang = lang;
    this.serviceLanguage.traslate(lang);
  }

  goToPage(url: string) {
    this.navigate.navigateByUrl(url);
  }

  getConfig() {
    this.serviceConfig.getConfig()
      .subscribe(
        (res: any) => {
          this.config_navbar = res.txt_navbar;
        }
      );
  }
}
