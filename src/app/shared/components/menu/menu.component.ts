import { FooterService } from './../footer/services/footer.service';
import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../services/shared.service';
import { Navigation } from '../../../interfaces/navigation';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { LanguageService } from '../../../services/language.service';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  menus: Navigation;
  pathUrl: string;
  lang: string = 'es';
  config_menu: any;
  user: any;
  token: any;
  logoFooterResponsive = "";

  constructor(
    private serviceLanguage: LanguageService,
    private serviceShared: SharedService,
    private navigate: Router,
    private location: Location,
    private serviceAuth: AuthService,
    private footerService: FooterService
  ) {
    this.user = localStorage.getItem('USER-DATA') ? JSON.parse(localStorage.getItem('USER-DATA')) : '';
  }

  ngOnInit(): void {
    this.lang = !sessionStorage.getItem('_LANG') ? 'es' : sessionStorage.getItem('_LANG');
    // console.log('this.lang :>> ', this.lang);
    this.postValidateToken();
    this.switchLang(this.lang);
    this.getNavigations();
    this.getConfig();
    this.pathUrl = this.location.path();
  }

  switchLang(lang) {
    this.lang = lang;
    this.serviceLanguage.traslate(lang);

    this.footerService.getLogo()
    .subscribe(
      (data:any) => {
        const { file } = data[0];
        this.logoFooterResponsive = 'http://159.223.201.87/'+file.route_footer;
      }
    )
  }

  // switchLang() {
  //   this.serviceLanguage.language.subscribe(
  //     (resp) => {
  //       this.lang = resp;
  //     }
  //   );
  // }

  getNavigations() {
    this.serviceShared
      .getNavigations()
      .subscribe(
        (res: Navigation) => {
          this.menus = res;
        }
      );
  }

  goToPage(url: string) {
    this.navigate.navigateByUrl(url);
    this.pathUrl = `/${url}`;
  }

  getConfig() {
    this.serviceShared.getConfig()
      .subscribe(
        (res: any) => {
          this.config_menu = res.txt_menu;
        }
      );
  }

  postValidateToken() {
    this.serviceAuth.postValidateToken(this.user.token).subscribe(
      (res: any) => {
        this.token = res;
        // console.log('postValidateToken res :>> ', res);
      },
      (err) => {
        this.token = false;
      }
    );
  }

}
