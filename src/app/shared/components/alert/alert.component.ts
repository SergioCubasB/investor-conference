import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LanguageService } from '../../../services/language.service';
import { SharedService } from '../../../services/shared.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  calendar: string = '';
  config_calendar: any;

  constructor(
    public dialogRef: MatDialogRef<AlertComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private serviceConfig: SharedService,
  ) { }

  ngOnInit(): void {
    this.getConfig();
  }

  getConfig() {
    this.serviceConfig.getConfig().subscribe(
      (res: any) => {
        this.config_calendar = res.txt_calendar;
      }
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  setCalendar(calendar: string) {
    this.calendar = calendar;
  }

  onClick(): void {
    if (this.calendar) {
      this.dialogRef.close(this.calendar);
      return;
    }
    this.dialogRef.close(true);
  }

}
