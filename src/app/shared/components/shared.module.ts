import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { OwlModule } from 'ngx-owl-carousel';

import { AlertComponent } from './alert/alert.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MenuComponent } from './menu/menu.component';
import { ViewdetailsComponent } from './viewdetails/viewdetails.component';
import { MaterialModule } from '../material/material.module';
import { MomentModule } from 'ngx-moment';
import { ViewdetaileventComponent } from './viewdetailevent/viewdetailevent.component';
import { PipesModule } from '../../pipes/pipes.module';
import { ViewinstructionsComponent } from './viewinstructions/viewinstructions.component';
import { BannerPrimaryComponent } from './banner-primary/banner-primary.component';


const components = [
  AlertComponent,
  FooterComponent,
  NavbarComponent,
  MenuComponent,
  ViewdetailsComponent,
  ViewdetaileventComponent,
  ViewinstructionsComponent,
  BannerPrimaryComponent
]

@NgModule({
  declarations: [
    components,
  ],
  imports: [
    CommonModule,
    CarouselModule,
    OwlModule,
    MaterialModule,
    MomentModule,
    PipesModule
  ],
  exports: [
    components
  ]
})
export class SharedModule { }
