import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewdetaileventComponent } from './viewdetailevent.component';

describe('ViewdetaileventComponent', () => {
  let component: ViewdetaileventComponent;
  let fixture: ComponentFixture<ViewdetaileventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewdetaileventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewdetaileventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
