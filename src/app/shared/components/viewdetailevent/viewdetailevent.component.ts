import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LanguageService } from '../../../services/language.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-viewdetailevent',
  templateUrl: './viewdetailevent.component.html',
  styleUrls: ['./viewdetailevent.component.scss']
})
export class ViewdetaileventComponent implements OnInit {

  lang: string = 'es';
  url = environment.host;

  constructor(
    public dialogRef: MatDialogRef<ViewdetaileventComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private serviceLanguage: LanguageService,
  ) { }

  ngOnInit(): void {
    // console.log('this.data evento :>> ', this.data);
  }

  switchLang() {
    this.serviceLanguage.language.subscribe(
      (resp) => {
        this.lang = resp;
      }
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
