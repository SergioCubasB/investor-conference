import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LanguageService } from '../../../services/language.service';

@Component({
  selector: 'app-viewinstructions',
  templateUrl: './viewinstructions.component.html',
  styleUrls: ['./viewinstructions.component.scss']
})
export class ViewinstructionsComponent implements OnInit {

  lang: string = 'es';

  constructor(
    public dialogRef: MatDialogRef<ViewinstructionsComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private serviceLanguage: LanguageService,
  ) { }

  ngOnInit(): void {
    this.switchLang();
  }

  switchLang() {
    this.serviceLanguage.language.subscribe(
      (resp) => {
        this.lang = resp;
      }
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
