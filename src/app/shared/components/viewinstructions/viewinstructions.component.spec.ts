import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewinstructionsComponent } from './viewinstructions.component';

describe('ViewinstructionsComponent', () => {
  let component: ViewinstructionsComponent;
  let fixture: ComponentFixture<ViewinstructionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewinstructionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewinstructionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
