import { ScheduleService } from './../../../services/schedule.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LanguageService } from '../../../services/language.service';
import { SharedService } from '../../../services/shared.service';
import { AuthService } from '../../../services/auth.service';
import { Navigation } from '../../../interfaces/navigation';
import { Location } from '@angular/common';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-banner-primary',
  templateUrl: './banner-primary.component.html',
  styleUrls: ['./banner-primary.component.scss']
})
export class BannerPrimaryComponent implements OnInit {
  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 600,
    navText: ['<img src="../../../../assets/icons/left.svg" />', '<img src="../../../../assets/icons/rigth.svg">'],
    responsive: {
      0: {
        items: 1 
      },
      400: {
        items: 1
      },
      760: {
        items: 1
      },
      1000: {
        items: 1
      }
    },
    nav: true
  }

  myInterval = 4000;
  activeSlideIndex = 0;
  pause = false;

  slides = [];

  pathUrl: string;
  menus: Navigation;
  lang = 'es';
  user: any;
  banner: any;
  public slider_es: any[] = [];
  public slider_en: any[] = [];

  public slider_static_en: any[] = [];
  public slider_static_es: any[] = [];
  
  constructor(
    private serviceLanguage: LanguageService,
    private serviceConfig: SharedService,
    private navigate: Router,
    private location: Location,
    private serviceAuth: AuthService,
    private sharedService: ScheduleService

  ) { }

  ngOnInit(): void {
    this.user = localStorage.getItem('USER-DATA') ? true : false;
    this.lang = sessionStorage.getItem('_LANG');
    this.switchLang();
    this.getNavigations();
    this.getConfig();
    this.pathUrl = this.location.path();
    this.slider(this.lang);


    this.sharedService.getSliders()
    .subscribe(
      (response: any) => {
        const  data   = response;
        
        data.forEach(element => {
          const { file } = element;

          if(element.type_head_id === 1){

            if(file.route){
              this.slider_es.push({ image : 'http://159.223.201.87/' + file.route});
            }
  
            if(file.route_en){
              this.slider_en.push({ image : 'http://159.223.201.87/' + file.route_en});
            }
            
          }

          if(element.type_head_id === 2){
            this.slider_static_en.push('http://159.223.201.87/' + file.route_en);
            this.slider_static_es.push('http://159.223.201.87/' + file.route);
          }
         
        });
      }
    )
  }

  switchLang() {
    this.serviceLanguage.language.subscribe(
      (resp) => {
        this.lang = resp;
        this.slider(this.lang);
      }
    );
  }

  getConfig() {
    this.serviceConfig.getConfig().subscribe(
      (res: any) => {
        this.banner = res.txtBanner;
      }
    );
  }


  goToPage(url: string) {
    this.navigate.navigateByUrl(url);
    this.pathUrl = `/${url}`;
  }

  getNavigations() {
    this.serviceConfig.getNavigations().subscribe(
      (res: Navigation) => {
        this.menus = res;
      }
    );
  }

  ngDoCheck() {
    this.user = localStorage.getItem('USER-DATA') ? true : false;
    this.pathUrl = this.location.path();
  }

  slider(lang) {
    if (lang === 'es') {
      this.slides = [
        { path: 'assets/img/slider/banner/Slider.png' },
        { path: 'assets/img/slider/banner/01.png' },
        { path: 'assets/img/slider/banner/02.png' },
        { path: 'assets/img/slider/banner/03.png' },
        { path: 'assets/img/slider/banner/04.png' },
        { path: 'assets/img/slider/banner/05.png' },
        { path: 'assets/img/slider/banner/06.png' },
        { path: 'assets/img/slider/banner/07.png' },
        { path: 'assets/img/slider/banner/08.png' },
      ];
    } else {
      this.slides = [
        { path: 'assets/img/slider/banner_en/Slider.png' },
        { path: 'assets/img/slider/banner_en/01.png' },
        { path: 'assets/img/slider/banner_en/02.png' },
        { path: 'assets/img/slider/banner_en/03.png' },
        { path: 'assets/img/slider/banner_en/04.png' },
        { path: 'assets/img/slider/banner_en/05.png' },
        { path: 'assets/img/slider/banner_en/06.png' },
        { path: 'assets/img/slider/banner_en/07.png' },
        { path: 'assets/img/slider/banner_en/08.png' },
      ];
    }
  }

}
