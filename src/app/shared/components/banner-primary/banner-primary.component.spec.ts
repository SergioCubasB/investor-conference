import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerPrimaryComponent } from './banner-primary.component';

describe('BannerPrimaryComponent', () => {
  let component: BannerPrimaryComponent;
  let fixture: ComponentFixture<BannerPrimaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BannerPrimaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerPrimaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
