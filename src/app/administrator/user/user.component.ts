import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import Swal from 'sweetalert2';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  users: any;

  constructor(
    private serviceUser: AuthService
  ) { }

  ngOnInit(): void {
    this.getUser();
  }

  getUser() {
    this.serviceUser.getUser().subscribe(
      (res) => {
        this.users = res;
        setTimeout(() => {
          Swal.close();
        }, 2000);
      }
    );
  }

  downloadReporte() {

    Swal.fire({
      title: 'Generando reporte, por favor espere...',
      allowEscapeKey: false,
      allowEnterKey: false,
      allowOutsideClick: false
    });
    Swal.showLoading();

    this.serviceUser.downloadUsers().subscribe(
      (res) => {

        let blob = new Blob([res], {
          type: 'application/octet-stream'
        });

        FileSaver.saveAs(blob, `ReporteUsuarios.xlsx`);

        // Swal.close();

        // console.log('blob :>> ', blob);
        Swal.fire({
          title: 'Descarga completa',
          icon: 'success'
        });
      },
      (err) => {
        console.log('error al descargar el reporte :>> ', err);
        Swal.fire({
          title: 'Error al descarga el archivo',
          icon: 'error'
        });
        Swal.close();
      },
      () => {
        Swal.close();
      }
    );
  }

}
