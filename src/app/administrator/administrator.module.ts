import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user/user.component';
import { AdministratorComponent } from './administrator.component';
import { AdministratorRoutingModule } from './administrator.routing';
import { SharedModule } from '../shared/components/shared.module';
import { ListSpectatorsComponent } from './list-spectators/list-spectators.component';



@NgModule({
  declarations: [
    AdministratorComponent,
    UserComponent,
    ListSpectatorsComponent,
  ],
  imports: [
    CommonModule,
    AdministratorRoutingModule,
    SharedModule,
  ]
})
export class AdministratorModule { }
