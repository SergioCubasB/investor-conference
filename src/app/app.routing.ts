import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './landing/register/register.component';
import { PagesComponent } from './landing/pages/pages.component';
import { OauthComponent } from './landing/oauth/oauth.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { NoregisterComponent } from './landing/noregister/noregister.component';
import { EventLiveComponent } from './landing/event-live/event-live.component';
import { ServiceConditionsComponent } from './landing/service-conditions/service-conditions.component';
import { PrivacyPolicyComponent } from './landing/privacy-policy/privacy-policy.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./landing/pages/pages.module').then(m => m.PagesModule),
      },
    ]
  },
  {
    path: 'administrator',
    component: AdministratorComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./administrator/administrator.module').then(m => m.AdministratorModule),
      },
    ]
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'oauth',
    component: OauthComponent
  },
  {
    path: 'noregister',
    component: NoregisterComponent
  },
  {
    path: 'event-live',
    component: EventLiveComponent
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent
  },
  {
    path: 'service-conditions',
    component: ServiceConditionsComponent
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
