import { Component } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'credicorpinvestor';

  constructor() {
    //this.loading();
  }

  async loading() {
    Swal.fire({
      title: 'Cargando ...',
      allowEscapeKey: false,
      allowEnterKey: false,
      allowOutsideClick: false
    });
    await Swal.showLoading();
  }
}
