import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertComponent } from '../../shared/components/alert/alert.component';
import { AuthService } from '../../services/auth.service';
import Swal from 'sweetalert2';
import { LanguageService } from 'src/app/services/language.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  formRegister: FormGroup;
  formInvalid = false;
  user: any;
  userNew: string;
  form = false;
  lang = 'es';

  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
    private route: Router,
    private serviceAuth: AuthService,
    private routeActivate: ActivatedRoute,
    private serviceLanguage: LanguageService,
  ) {
    // console.log('language :>> ', routeActivate.snapshot.queryParams.lang );
    this.user = localStorage.getItem('USER-DATA') ? JSON.parse(localStorage.getItem('USER-DATA')) : '';
    this.userNew = localStorage.getItem('USER-DATA-EVENT') ? JSON.parse(localStorage.getItem('USER-DATA-EVENT')) : '';
    this.lang = routeActivate.snapshot.queryParams.lang ? routeActivate.snapshot.queryParams.lang : 'es';
  }

  ngOnInit(): void {
    this.switchLang(this.lang);
    this.postValidateToken();
  }

  switchLang(lang) {
    this.lang = lang;
    this.serviceLanguage.traslate(lang);
  }

  loadForm() {
    this.formRegister = this.fb.group(
      {
        name: [this.user ? this.user.name : '', [Validators.required]],
        last_name: [this.user ? this.user.last_name : '', [Validators.required]],
        email: [this.user ? this.user.email : '', [Validators.required, Validators.pattern('[aA-zZ0-9._%+-]+@[aA-zZ0-9.-]+\.[aA-zZ]{2,3}$')]]
      }
    );
    setTimeout(() => {
      Swal.close();
    }, 2000);
  }

  onSubmit() {

    if (this.formRegister.invalid) {
      this.formInvalid = true;
      return false;
    }

    const formData = new FormData();

    formData.set('name', this.formRegister.value.name);
    formData.set('last_name', this.formRegister.value.last_name);
    formData.set('email', this.formRegister.value.email);

    formData.set('lang', this.lang === 'es' ? '1' : '2');

    // formData.set('type_user ', '1');
    this.serviceAuth.postRegiste(formData)
      .subscribe(
        (res: any) => {
          if (res.status === 200) {
            this.openDialog({
              image: 'assets/icons/hecho2.svg',
              // title: '¡Registro exitoso!',
              title: this.lang === 'es' ? '¡Se ha registrado exitosamente en el evento!' : 'You have successfully registered for the event!',
              optionalText: this.lang === 'es' ? 'Para agregar las conferencias en su calendario, haga clic en el siguiente botón:' : 'To add the conferences to your calendar, click the following button:',
              btnAccept: this.lang === 'es' ? 'Agendar conferencias' : 'Schedule conferences',
              btnCancel: '',
            });
            localStorage.setItem('USER-DATA', JSON.stringify(res.data));
          }
          else if (res.status === 201) {
            this.openDialog({
              image: 'assets/icons/hecho2.svg',
              // title: '¡Registro exitoso!',
              title: this.lang === 'es' ? 'Usted ya se encuentra registrado' : 'You are already registered',
              optionalText: this.lang === 'es' ? 'Para agregar las conferencias en su calendario, haga clic en el siguiente botón:' : 'To add the conferences to your calendar, click the following button:',
              btnAccept: this.lang === 'es' ? 'Agendar conferencias' : 'Schedule conferences',
              btnCancel: '',
            });
            localStorage.setItem('USER-DATA', JSON.stringify(res.data));
          }
          else {
            this.openDialog({
              // image: 'assets/icons/hecho.svg',
              title: this.lang === 'es' ? '¡Lo sentimos..!' : 'We are sorry..!',
              text: this.lang === 'es' ? 'Ha ocurrido un error al registrar' : 'An error occurred while registering',
              // optionalText: '¡Gracias por su participación!',
              btnAccept: '',
              btnCancel: this.lang === 'es' ? 'Aceptar' : 'To accept',
            });
          }
        },
        (err) => {
          this.openDialog({
            // image: 'assets/icons/hecho.svg',
            title: this.lang === 'es' ? '¡Lo sentimos..!' : 'We are sorry..!',
            text: this.lang === 'es' ? 'Ha ocurrido un error al registrar' : 'An error occurred while registering',
            // optionalText: '¡Gracias por su participación!',
            btnAccept: '',
            btnCancel: this.lang === 'es' ? 'Aceptar' : 'To accept',
          });
        }
      );



  }

  openDialog(message: any) {
    const dialogRef = this.dialog.open(AlertComponent, {
      // width: '250px',
      // height: '55vh',
      disableClose: true,
      data: message
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('ir', result);
      if (result) {
        // console.log('result :>> ', result);
        if (this.userNew) {
          // console.log('ths.userNew :>> ', this.userNew);
          localStorage.removeItem('USER-DATA-EVENT');
          this.route.navigateByUrl('/event-live');
          return;
        }
        this.route.navigateByUrl('/schedule');

      }
    });
  }

  postValidateToken() {
    this.serviceAuth.postValidateToken(this.user.token).subscribe(
      (res: any) => {
        // console.log('ok', res);

        this.route.navigateByUrl('schedule');
      },
      (err) => {
        console.log('eror', err);
        this.form = !this.form;
        this.loadForm();
      }
    );
  }


}
