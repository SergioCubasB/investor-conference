import { Component, HostListener, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { ScheduleService } from '../../services/schedule.service';
import { LanguageService } from '../../services/language.service';
import { Schedule } from '../../interfaces/schedule';
import { SharedService } from '../../services/shared.service';
import * as moment from 'moment-timezone';
import { MatDialog } from '@angular/material/dialog';
import { ViewdetaileventComponent } from '../../shared/components/viewdetailevent/viewdetailevent.component';
import { asapScheduler } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-event-live',
  templateUrl: './event-live.component.html',
  styleUrls: ['./event-live.component.scss']
})
export class EventLiveComponent implements OnInit {

  user: any;
  lang = 'es';
  schedules: Schedule[];
  mySchedules: Schedule[];
  mySchedulesSearch: Schedule[];
  myAgenda: Schedule;
  agendasearch = [];
  config_agenda: any;
  config_schedule: any;
  day = moment().format('YYYY-MM-DD');
  hour = moment().tz("America/Lima").format('YYYY-MM-DD HH:mm:ss');
  time = 75; // tiempo en minutos para mostrar transmisión
  title: string;
  title_en: string;
  event = true;
  userViewer: any;
  eventLive: any = '';
  msj = false;
  vimeo = true;
  youtube = false;

  formQuestions: FormGroup;

  @HostListener('window:beforeunload')
  beforeunload(e) {
    // window.open('http://localhost:4200');
    // this.postSpectator(this.userViewer.id, this.eventLive);
    // console.log('hacer algo :>> ', e);
    asapScheduler.schedule(() => {
      localStorage.setItem('asap', 'asap');
      const formData = new FormData();
      formData.append('user_id', this.user.id);
      formData.append('date_', moment().format('YYYY-MM-DD'));
      formData.append('hour_', moment().format('HH:mm:ss'));
      formData.append('event_id', '');
      formData.append('id', `${this.userViewer.id}`);

      this.serviceAuth.postSpectator(formData).subscribe(
        (res: any) => {
          res;
          // console.log('res postSpectator:>> ', res);
          res.idSpectator = this.userViewer.id;
          res.tipo = 'Salida';
          localStorage.setItem('res', JSON.stringify(res));
        },
        (err) => {
          console.log('err postSpectator:>> ', err);
        },
      );
    });
    localStorage.setItem('espectador', 'salio a la transmisión cerrando la pestaña');

  }

  constructor(
    private route: Router,
    private serviceAuth: AuthService,
    private serviceSchedule: ScheduleService,
    private serviceLanguage: LanguageService,
    private serviceConfig: SharedService,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private routeActivate: ActivatedRoute,
  ) {
    this.user = localStorage.getItem('USER-DATA') ? JSON.parse(localStorage.getItem('USER-DATA')) : '';
    // this.lang = routeActivate.snapshot.queryParams.lang ? routeActivate.snapshot.queryParams.lang : 'es';
    // sessionStorage.setItem('_LANG', this.lang);
  }

  ngOnInit(): void {

    this.formQuestions = this.fb.group({
      question: ['']
    });

    this.postValidateToken();
    this.lang = sessionStorage.getItem('_LANG');

    window.addEventListener("beforeunload", function (e) {
      var confirmationMessage = 'Estas seguro de cerrar la ventana';
      // console.log('cond');
      e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
      return confirmationMessage;              // Gecko, WebKit, Chrome <34
    });

    this.getConfig();
    this.switchLang();
    this.getSchedule(this.user.token);
    this.getMySchedule();
    setInterval(() => {
      this.day = moment().format('YYYY-MM-DD');
      this.hour = moment().tz("America/Lima").format('YYYY-MM-DD HH:mm:ss');

      if (this.lang === 'es') {
        this.getLiveVimeo('1164381');
      } else {
        this.getLiveVimeo('1164386');
      }

      // this.getLiveYoutube('');

    }, 1000);

    setInterval(() => {
      this.schedules.forEach((e) => {
        e.events.forEach((eve) => {
          if (moment(this.hour).diff(eve.date) > 0) {
            // console.log('this.schedules :>> ', eve, moment(this.hour).diff(eve.date));
            this.title = eve.title;
            this.title_en = eve.title_en;
            // this.eventLive = eve.id;
          }

        });
      });
    }, 1000);

    this.postSpectator(0, this.eventLive);
    localStorage.setItem('espectador', 'entro a la transmisión');

  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    // console.log('object :>> onDestroy');
    if (this.userViewer) {
      this.postSpectator(this.userViewer.id, this.eventLive);
    }
    localStorage.setItem('espectador', 'salio a la transmisión');
    // this.putSpectator();
  }

  showEvent(event: boolean) {
    this.event = event;
  }

  postValidateToken() {
    this.serviceAuth.postValidateToken(this.user.token).subscribe(
      (res: any) => {
        // console.log('postValidateToken', res);
      },
      (err) => {
        localStorage.setItem('USER-DATA-EVENT', 'true');
        this.route.navigateByUrl('register');
        console.log('eror postValidateToken', err);
        return;
      }
    );
  }

  switchLang() {
    this.serviceLanguage.language.subscribe(
      (resp) => {
        this.lang = resp;
      }
    );
  }

  getConfig() {
    this.serviceConfig.getConfig()
      .subscribe(
        (res: any) => {
          this.config_schedule = res.txt_schedule;
          this.config_agenda = res.txt_agenda;
        }
      );
  }

  getSchedule(token) {
    this.serviceSchedule
      .getSchedule(token)
      .subscribe(
        (res: Schedule[]) => {
          this.schedules = res;
          // console.log('this.schedules live:>> ', this.schedules);
          setTimeout(() => {
            Swal.close();
          }, 2000);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getMySchedule() {
    this.serviceSchedule.getMySchedule(this.user.token)
      .subscribe(
        (res: Schedule[]) => {
          this.mySchedules = res;
          this.mySchedulesSearch = res;
          setTimeout(() => {
            Swal.close();
          }, 2000);
        }
      );
  }

  searchAgenda(text: string) {
    if (text.toString().length) {
      let even: any

      this.mySchedulesSearch.forEach(
        (agenda, index) => {
          even = agenda.events.filter(
            (evento) => {
              if (this.lang === 'es') {
                return evento.title.toLowerCase().indexOf(text.toLowerCase()) > -1;
              } else {
                return evento.title_en.toLowerCase().indexOf(text.toLowerCase()) > -1;
              }

            }
          );

          if (even) {
            this.myAgenda = {
              id: this.mySchedulesSearch[index].id,
              date: this.mySchedulesSearch[index].date,
              date_string: this.mySchedulesSearch[index].date_string,
              date_string_en: this.mySchedulesSearch[index].date_string_en,
              date_string_large: this.mySchedulesSearch[index].date_string_large,
              date_string_large_en: this.mySchedulesSearch[index].date_string_large_en,
              events: even
            };
            this.agendasearch.push(this.myAgenda);
          }
        }
      );
      this.mySchedules = this.agendasearch;
      this.agendasearch = [];
      return;
    }
    this.getMySchedule();
    Swal.close();
  }

  viewDetailEvent(idEvent: number) {
    this.serviceSchedule.getEvent(idEvent).subscribe(
      (res: any) => {
        // console.log(res);
        const dialogRef = this.dialog.open(ViewdetaileventComponent, {
          // width: '250px',
          // height: '90vh',
          disableClose: true,
          data: {
            lang: this.lang,
            info: res,
            agenda: res.EventsbyModerator,
          }
        });
      }
    );
  }

  postSpectator(idSpectator: number, idEvent) {
    const formData = new FormData();
    formData.append('user_id', this.user.id);
    formData.append('date_', moment().format('YYYY-MM-DD'));
    formData.append('hour_', moment().format('HH:mm:ss'));
    formData.append('event_id', idEvent);
    formData.append('id', `${idSpectator}`);

    this.serviceAuth.postSpectator(formData).subscribe(
      (res: any) => {
        this.userViewer = res;
        // console.log('res postSpectator:>> ', res);
        this.userViewer.idSpectator = idSpectator;
        this.userViewer.tipo = 'Entrada';
        localStorage.setItem('res', JSON.stringify(this.userViewer));
      },
      (err) => {
        console.log('err postSpectator:>> ', err);
      },
    );
  }

  putSpectator() {
    localStorage.setItem('salio', 'salio a la transmisión');
    localStorage.removeItem('entro');
    const data = {
      final_hour: moment().format('HH:mm:ss')
    };
    this.serviceAuth.putSpectator(this.userViewer.id, data).subscribe(
      (res: any) => {
        // console.log('res putSpectator:>> ', res);
      },
      (err) => {
        console.log('err putSpectator:>> ', err);
      },
    );
  }

  sendQuestion() {
    // console.log('pregunta:>>', this.formQuestions.value);
    const formData = new FormData();
    formData.append('token', this.user.token);
    formData.append('question', this.formQuestions.value.question);
    formData.append('date_', moment().format('YYYY-MM-DD'));
    formData.append('hour_', moment().format('HH:mm:ss'));
    formData.append('lang', '1');

    this.serviceSchedule.postQuestions(formData).subscribe(
      (res: any) => {
        if (res.status === 200) {
          this.msj = true;
          setTimeout(() => {
            this.formQuestions.reset();
            this.msj = false;
          }, 2000);
          return;
        }
      }
    );
  }

  getLiveVimeo(id) {
    this.serviceSchedule.getLiveVimeo(id).subscribe(
      (res: any) => {
        // console.log('video vimeo:>> ', id, res);
        if (res.domain_status_code !== 200) {
          this.vimeo = false;
          this.youtube = true;
        } else {
          this.vimeo = true;
          this.youtube = false;
        }
      }
    );
  }

  getLiveYoutube(id) {
    this.serviceSchedule.getLiveYoutube(id).subscribe(
      (res) => {
        // console.log('video youtube :>> ', res);
      }
    );
  }

}
