import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderComponent } from './slider/slider.component';
import { BannerComponent } from './banner/banner.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { OwlModule } from 'ngx-owl-carousel';

var components = [
  SliderComponent,
  BannerComponent
]

@NgModule({
  declarations: [
    components
  ],
  imports: [
    CommonModule,
    CarouselModule,
    OwlModule,
  ],
  exports: [
    components
  ]
})
export class ComponentsModule { }
