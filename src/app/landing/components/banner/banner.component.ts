import { ScheduleService } from './../../../services/schedule.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LanguageService } from '../../../services/language.service';
import { SharedService } from '../../../services/shared.service';
import { AuthService } from '../../../services/auth.service';
import { Navigation } from '../../../interfaces/navigation';
import { Location } from '@angular/common';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  myInterval = 4000;
  activeSlideIndex = 0;
  pause = false;

  slides = [];

  pathUrl: string;
  menus: Navigation;
  lang = 'es';
  user: any;
  banner: any;
  public slider_es: any[] = [];
  public slider_en: any[] = [];

  constructor(
    private serviceLanguage: LanguageService,
    private serviceConfig: SharedService,
    private navigate: Router,
    private location: Location,
    private serviceAuth: AuthService,
    private sharedService: ScheduleService

  ) { }

  ngOnInit(): void {
    this.user = localStorage.getItem('USER-DATA') ? true : false;
    this.lang = sessionStorage.getItem('_LANG');
    this.switchLang();
    this.getNavigations();
    this.getConfig();
    this.pathUrl = this.location.path();
    this.slider(this.lang);


    this.sharedService.getSliders()
    .subscribe(
      (response: any) => {
        const  data   = response;
        
        data.forEach(element => {
          const { file } = element;
          if(file.route){
            this.slider_es.push({ image : 'http://159.223.201.87/media/' + file.route});
          }

          if(file.route_en){
            this.slider_en.push({ image : 'http://159.223.201.87/media/' + file.route_en});
          }
        });

        console.log("SLIDER:",this.slider_en);

        
      }
    )
  }

  switchLang() {
    this.serviceLanguage.language.subscribe(
      (resp) => {
        this.lang = resp;
        this.slider(this.lang);
      }
    );
  }

  getConfig() {
    this.serviceConfig.getConfig().subscribe(
      (res: any) => {
        this.banner = res.txtBanner;
      }
    );
  }


  goToPage(url: string) {
    this.navigate.navigateByUrl(url);
    this.pathUrl = `/${url}`;
  }

  getNavigations() {
    this.serviceConfig.getNavigations().subscribe(
      (res: Navigation) => {
        this.menus = res;
      }
    );
  }

  ngDoCheck() {
    this.user = localStorage.getItem('USER-DATA') ? true : false;
    this.pathUrl = this.location.path();
  }

  slider(lang) {
    if (lang === 'es') {
      this.slides = [
        { path: 'assets/img/slider/banner/Slider.png' },
        { path: 'assets/img/slider/banner/01.png' },
        { path: 'assets/img/slider/banner/02.png' },
        { path: 'assets/img/slider/banner/03.png' },
        { path: 'assets/img/slider/banner/04.png' },
        { path: 'assets/img/slider/banner/05.png' },
        { path: 'assets/img/slider/banner/06.png' },
        { path: 'assets/img/slider/banner/07.png' },
        { path: 'assets/img/slider/banner/08.png' },
      ];
    } else {
      this.slides = [
        { path: 'assets/img/slider/banner_en/Slider.png' },
        { path: 'assets/img/slider/banner_en/01.png' },
        { path: 'assets/img/slider/banner_en/02.png' },
        { path: 'assets/img/slider/banner_en/03.png' },
        { path: 'assets/img/slider/banner_en/04.png' },
        { path: 'assets/img/slider/banner_en/05.png' },
        { path: 'assets/img/slider/banner_en/06.png' },
        { path: 'assets/img/slider/banner_en/07.png' },
        { path: 'assets/img/slider/banner_en/08.png' },
      ];
    }
  }

}
