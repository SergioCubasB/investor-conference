import { Component, Input, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  @Input() myInterval = 5000;
  @Input() activeSlideIndex = 0;
  @Input() pause = false;
  @Input() images = [];

  @Input() height: any;
  @Input() cellWidth: any;
  // @Input() altura
  // @Input() flechas
  @Input() autoplay = false;
  @Input() arrowsOutside = false;
  @Input() dots = true;
  @Input() cellstoShow = 1;
  @Input() ngClass = false;


  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      // console.log('this.images :>> ', this.images);
    }, 1000);
  }

}
