import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-noregister',
  templateUrl: './noregister.component.html',
  styleUrls: ['./noregister.component.scss']
})
export class NoregisterComponent implements OnInit {

  param: any;
  lang = 'es';

  constructor(
    public dialog: MatDialog,
    private route: Router,
    private serviceAuth: AuthService,
    private routeActivated: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    // window.close();
    Swal.close();
    // setTimeout(() => {
    //   window.close();
    //   console.log('ventana ceerrada');
    // }, 10000);

    this.routeActivated.queryParams.subscribe(
      (res) => {
        this.param = res;
        this.lang = this.param.lang ? this.param.lang : 'es';
        // console.log(res);
        // console.log('Usuario no asistirá');
        setTimeout(() => {
          localStorage.clear();
          this.getNoRegister();
        }, 1000);
      }
    );
  }

  openDialog(message: any) {
    const dialogRef = this.dialog.open(NoregisterComponent, {
      // width: '250px',
      height: '55vh',
      disableClose: true,
      data: message
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log('ir', result);
      if (result) {
        this.route.navigateByUrl('/');
      }
    });
  }

  getNoRegister() {
    this.serviceAuth.getNoRegister(this.param).subscribe(
      () => {
        setTimeout(() => {
          window.close();
          // console.log('ventana cerrada');
        }, 30 * 1000);
      }
    )
  }

}
