import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoregisterComponent } from './noregister.component';

describe('NoregisterComponent', () => {
  let component: NoregisterComponent;
  let fixture: ComponentFixture<NoregisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoregisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
