import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ScheduleService } from '../../services/schedule.service';

@Component({
  selector: 'app-oauth',
  templateUrl: './oauth.component.html',
  styleUrls: ['./oauth.component.scss']
})
export class OauthComponent implements OnInit {

  codeClendar: string;
  user: any;
  calendar: string;

  constructor(
    private routeActivated: ActivatedRoute,
    private serviceSchedule: ScheduleService,
  ) {
    this.user = localStorage.getItem('USER-DATA') ? JSON.parse(localStorage.getItem('USER-DATA')) : {};
    this.calendar = sessionStorage.getItem('_CALENDAR') ? sessionStorage.getItem("_CALENDAR") : null;
  }

  ngOnInit(): void {
    this.routeActivated.queryParams.subscribe(
      (res) => {
        this.codeClendar = res.code
        setTimeout(() => {
          this.scheduleCalendar();
        }, 1000);
      }
    );
  }

  async scheduleCalendar() {
    const formData = new FormData();

    formData.append('code', this.codeClendar);
    formData.append('userId', this.user.id);
    formData.append('tokenUser', this.user.token);

    await this.serviceSchedule.postCalendar(this.calendar, formData).subscribe(
      (res: any) => {
        setTimeout(() => {
          window.open(res, '_blank');
          window.close();
        }, 1000);
      }
    );

    sessionStorage.removeItem('_CALENDAR');

  }

}
