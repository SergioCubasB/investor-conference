import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import Swal from 'sweetalert2';
import { LanguageService } from '../../../services/language.service';
import { CredicorpService } from '../credicorp/services/credicorp.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {
  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 600,
    navText: ['<img src="../../../../assets/icons/left.svg" />', '<img src="../../../../assets/icons/rigth.svg">'],
    responsive: {
      0: {
        items: 1 
      },
      400: {
        items: 1
      },
      760: {
        items: 1
      },
      1000: {
        items: 1
      }
    },
    nav: true
  }

  myInterval = 4000;
  activeSlideIndex = 0;
  pause = false;
  lang = 'es';
  data: [] = [];
  dataExternal: [] = [];
  dataAttribute: [] = [];
  nuevoArray = [];
  arrayTemporal = [];
  
  date_number:number = 0;
  date_mouth = '';

  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  event2018 = [
    { path: 'assets/img/events/2018/01.jpg' },
    { path: 'assets/img/events/2018/02.jpg' },
    { path: 'assets/img/events/2018/03.jpg' },
    { path: 'assets/img/events/2018/04.jpg' },
    { path: 'assets/img/events/2018/05.jpg' },
    { path: 'assets/img/events/2018/06.jpg' },
    { path: 'assets/img/events/2018/07.jpg' },
    { path: 'assets/img/events/2018/08.jpg' },
    { path: 'assets/img/events/2018/09.jpg' },
    { path: 'assets/img/events/2018/10.jpg' },
    { path: 'assets/img/events/2018/11.jpg' },
    { path: 'assets/img/events/2018/12.jpg' },
    { path: 'assets/img/events/2018/13.jpg' },
    { path: 'assets/img/events/2018/14.jpg' },
    { path: 'assets/img/events/2018/15.jpg' },
    { path: 'assets/img/events/2018/16.jpg' },
    { path: 'assets/img/events/2018/17.jpg' },
    { path: 'assets/img/events/2018/18.jpg' },
    { path: 'assets/img/events/2018/19.jpg' },
    { path: 'assets/img/events/2018/20.jpg' },
    { path: 'assets/img/events/2018/21.jpg' },
    { path: 'assets/img/events/2018/22.jpg' },
    { path: 'assets/img/events/2018/23.jpg' },
    { path: 'assets/img/events/2018/24.jpg' },
    { path: 'assets/img/events/2018/25.jpg' },
    { path: 'assets/img/events/2018/26.jpg' },
    { path: 'assets/img/events/2018/27.jpg' },
    { path: 'assets/img/events/2018/28.jpg' },
    { path: 'assets/img/events/2018/29.jpg' },
    { path: 'assets/img/events/2018/30.jpg' },
    { path: 'assets/img/events/2018/31.jpg' },
    { path: 'assets/img/events/2018/32.jpg' },
    { path: 'assets/img/events/2018/33.jpg' },
    { path: 'assets/img/events/2018/34.jpg' },
    { path: 'assets/img/events/2018/35.jpg' },
    { path: 'assets/img/events/2018/36.jpg' },
    { path: 'assets/img/events/2018/37.jpg' },
    { path: 'assets/img/events/2018/38.jpg' },
    { path: 'assets/img/events/2018/39.jpg' },
    { path: 'assets/img/events/2018/40.jpg' },
    { path: 'assets/img/events/2018/41.jpg' },
    { path: 'assets/img/events/2018/42.jpg' },
    { path: 'assets/img/events/2018/43.jpg' },
    { path: 'assets/img/events/2018/44.jpg' },
    { path: 'assets/img/events/2018/45.jpg' },
    { path: 'assets/img/events/2018/46.jpg' },
    { path: 'assets/img/events/2018/47.jpg' },
    { path: 'assets/img/events/2018/48.jpg' },
    { path: 'assets/img/events/2018/49.jpg' },
    { path: 'assets/img/events/2018/50.jpg' },
    { path: 'assets/img/events/2018/51.jpg' },
    { path: 'assets/img/events/2018/52.jpg' },
    { path: 'assets/img/events/2018/53.jpg' },
    { path: 'assets/img/events/2018/54.jpg' },
  ];

  event2019 = [
    { path: 'assets/img/events/2019/01.jpg' },
    { path: 'assets/img/events/2019/02.jpg' },
    { path: 'assets/img/events/2019/03.jpg' },
    { path: 'assets/img/events/2019/04.jpg' },
    { path: 'assets/img/events/2019/05.jpg' },
    { path: 'assets/img/events/2019/06.jpg' },
    { path: 'assets/img/events/2019/07.jpg' },
    { path: 'assets/img/events/2019/08.jpg' },
    { path: 'assets/img/events/2019/09.jpg' },
    { path: 'assets/img/events/2019/10.jpg' },
    { path: 'assets/img/events/2019/11.jpg' },
    { path: 'assets/img/events/2019/12.jpg' },
    { path: 'assets/img/events/2019/13.jpg' },
    { path: 'assets/img/events/2019/14.jpg' },
    { path: 'assets/img/events/2019/15.jpg' },
    { path: 'assets/img/events/2019/16.jpg' },
    { path: 'assets/img/events/2019/17.jpg' },
    { path: 'assets/img/events/2019/18.jpg' },
    { path: 'assets/img/events/2019/19.jpg' },
    { path: 'assets/img/events/2019/20.jpg' },
    { path: 'assets/img/events/2019/21.jpg' },
    { path: 'assets/img/events/2019/22.jpg' },
    { path: 'assets/img/events/2019/23.jpg' },
    { path: 'assets/img/events/2019/24.jpg' },
    { path: 'assets/img/events/2019/25.jpg' },
    { path: 'assets/img/events/2019/26.jpg' },
    { path: 'assets/img/events/2019/27.jpg' },
    { path: 'assets/img/events/2019/28.jpg' },
  ];

  event2020 = [
    { path: 'assets/img/events/2020/01.png' },
    { path: 'assets/img/events/2020/02.png' },
    { path: 'assets/img/events/2020/03.png' },
    { path: 'assets/img/events/2020/04.png' },
    { path: 'assets/img/events/2020/05.png' },
    { path: 'assets/img/events/2020/06.png' },
    { path: 'assets/img/events/2020/07.png' },
    { path: 'assets/img/events/2020/08.png' },
  ];

  constructor(
    private serviceLanguage: LanguageService,
    private credicorpService: CredicorpService,
    private sanitizer : DomSanitizer
  ) { }

  ngOnInit(): void {
    this.lang = sessionStorage.getItem('_LANG');
    this.switchLang();
    this.loading();
    setTimeout(() => {
      Swal.close();
    }, 1000);
  }

  async loading() {
    Swal.fire({
      title: 'Cargando ...',
      allowEscapeKey: false,
      allowEnterKey: false,
      allowOutsideClick: false
    });
    await Swal.showLoading();
  }

  switchLang() {
    this.serviceLanguage.language.subscribe(
      (resp) => {
        this.lang = resp;
      }
    );

    
    this.credicorpService.getPageEvent()
    .subscribe(
      (response: any) => {
        const  data  = response;
        this.data = data;
      }
    )

    this.credicorpService.getDate()
    .subscribe(
      (response: any) => {
        const  data = response;
        this.formatDate(data);
      }
    )

    this.credicorpService.getAttribute(2)
    .subscribe(
      (response: any) => {
        for(var i=0; i<response.length; i++){
          this.arrayTemporal = this.nuevoArray.filter(resp => resp["atr"] == response[i]["type_attribute_id"])
          if(this.arrayTemporal.length>0){
              this.nuevoArray[this.nuevoArray.indexOf(this.arrayTemporal[0])]["Sliders"].push( { image : 'http://159.223.201.87/'+response[i]["file"].route})
          }else{
              this.nuevoArray.push({"atr" : response[i]["type_attribute_id"], "title" : response[i]["atr"] , "Sliders" : [ { image : 'http://159.223.201.87/'+response[i]["file"].route}]})
          }
      }
        
      }
    )

    this.credicorpService.getAttributeExternal(2)
    .subscribe(
      (response: any) => {
        const  data  = response;
        this.dataExternal = data;
      }
    )
  }

  executeData(){
    
  }

  goToLink(file: string) {
    window.open(file, '_blank');
    //window.open(`assets/files/${file}.pdf`, '_blank');
  }

  formatDate(date: any){
    const dateFormat = new Date(date[0].start_date);

    const day = date[0].start_date.slice(-2);
    const mouth = dateFormat.toLocaleString('default', { month: 'long' }).slice(0,3);

    this.date_number =  day;
    this.date_mouth = mouth;
  }

}
