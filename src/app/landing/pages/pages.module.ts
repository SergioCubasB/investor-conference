import { NgModule } from '@angular/core';

import { CarouselModule } from 'ngx-owl-carousel-o';
import { OwlModule } from 'ngx-owl-carousel';

import { CommonModule } from '@angular/common';
import { MomentModule } from 'ngx-moment';
import { ScheduleComponent } from './schedule/schedule.component';
import { PageRoutingModule } from './pages.routing';
import { PagesComponent } from './pages.component';
import { SharedModule } from '../../shared/components/shared.module';
import { CredicorpComponent } from './credicorp/credicorp.component';
import { ComponentsModule } from '../components/components.module';
import { SpeakerComponent } from './speaker/speaker.component';
import { EventComponent } from './event/event.component';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ScheduleComponent,
    PagesComponent,
    CredicorpComponent,
    SpeakerComponent,
    EventComponent,
  ],
  imports: [
    CommonModule,
    
    CarouselModule,
    OwlModule,

    PageRoutingModule,
    SharedModule,
    MomentModule,
    ComponentsModule,
    PipesModule,
  ]
})
export class PagesModule { }
