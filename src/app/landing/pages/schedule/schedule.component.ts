import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AlertComponent } from '../../../shared/components/alert/alert.component';
import { LanguageService } from '../../../services/language.service';
import { ScheduleService } from '../../../services/schedule.service';
import { SharedService } from '../../../services/shared.service';
import { Schedule } from '../../../interfaces/schedule';
import 'moment';
import * as moment from 'moment-timezone';
import Swal from 'sweetalert2';
import { environment } from '../../../../environments/environment';
import { ViewdetailsComponent } from '../../../shared/components/viewdetails/viewdetails.component';
import { ViewdetaileventComponent } from '../../../shared/components/viewdetailevent/viewdetailevent.component';
import { ViewinstructionsComponent } from '../../../shared/components/viewinstructions/viewinstructions.component';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {

  lang: string = 'es';
  schedules: Schedule[];
  mySchedules: Schedule[];
  mySchedulesSearch: Schedule[];
  myScheduleCalendar = [];
  myAgendaCalendar = [];
  myAgenda: Schedule;
  agendasearch = [];

  config_schedule: any;
  config_agenda: any;

  day = moment().format('YYYY-MM-DD');
  hour = moment().tz("America/Lima").format('YYYY-MM-DD HH:mm:ss');
  time = 75; // tiempo en minutos para mostrar transmisión

  user: any;
  token: any;

  addEvent_ClientID = environment.clientId_addEvent;

  url = environment.host;

  constructor(
    private serviceSchedule: ScheduleService,
    private serviceLanguage: LanguageService,
    private serviceConfig: SharedService,
    public dialog: MatDialog,
    private route: Router,
    private serviceAuth: AuthService,
  ) {
    this.user = localStorage.getItem('USER-DATA') ? JSON.parse(localStorage.getItem('USER-DATA')) : '';
  }

  ngOnInit(): void {
    this.lang = sessionStorage.getItem('_LANG');
    this.postValidateToken();
    this.getSchedule(this.user.token);
    this.getMySchedule();
    this.getConfig();
    this.switchLang();
    this.loading();
    setInterval(() => {
      this.day = moment().format('YYYY-MM-DD');
      this.hour = moment().tz("America/Lima").format('YYYY-MM-DD HH:mm:ss');
    }, 1000);
  }

  async loading() {
    Swal.fire({
      title: 'Cargando ...',
      allowEscapeKey: false,
      allowEnterKey: false,
      allowOutsideClick: false
    });
    await Swal.showLoading();
  }

  postValidateToken() {
    this.serviceAuth.postValidateToken(this.user.token).subscribe(
      (res: any) => {
        this.viewInstructions();
        this.token = res;
        // console.log('postValidateToken res :>> ', res);
      },
      (err) => {
        // console.log('this.user :>> ', this.user);
        if (this.user) {
          // console.log('this.user err:>> ', this.user);
          this.route.navigateByUrl('register');
        }
        this.token = false;
      }
    );
  }

  getConfig() {
    this.serviceConfig.getConfig()
      .subscribe(
        (res: any) => {
          this.config_schedule = res.txt_schedule;
          this.config_agenda = res.txt_agenda;
        }
      );
  }

  switchLang() {
    this.serviceLanguage.language.subscribe(
      (resp) => {
        this.lang = resp;
      }
    );
  }

  getSchedule(token) {
    this.serviceSchedule.getSchedule(token).subscribe(
      (res: any) => {
        this.schedules = res;
        setTimeout(() => {
          Swal.close();
        }, 2000);
      },
      (err) => {
      }
    );
  }

  goSchedule() {
    this.openDialog({
      calendar: true,
      lang: this.lang,
      image: '',
      title: '',
      text: '',
      optionalText: '',
      btnAccept: this.lang === 'es' ? 'Continuar' : 'Continue',
      btnCancel: '',
    });
  }

  openDialog(message: any) {
    const dialogRef = this.dialog.open(AlertComponent, {
      // width: '250px',
      // height: '65vh',
      disableClose: true,
      data: message
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loading();
        this.goMyAgenda(result);
      }
    });
  }

  addScheduleCalendar(event: any) {

    const element = document.getElementById(`schedule-${event.id}`);
    const pos = this.myScheduleCalendar.indexOf(event.id);

    if (pos > -1) {
      // TODO descomentar para que funcione con los check
      // element.classList.remove('checked');
      this.myScheduleCalendar.splice(pos, 1);
      this.myAgendaCalendar.splice(pos, 1);
      return;
    }
    this.myScheduleCalendar.push(event.id);
    this.myAgendaCalendar.push(event);
    // TODO descomentar para que funcione con los check
    // element.classList.add('checked');
    this.goSchedule();

  }

  cleanScheduleCalendar() {
    this.myScheduleCalendar = [];
    const checked = document.getElementsByClassName('add-schedule');
    for (let index = 0; index < checked.length; index++) {
      const element = checked[index];
      element.classList.remove('checked');
    }
  }

  downloadPresentation(presentation: string) {

    var link = document.createElement("a");
    link.download = "Presentación";
    link.target = "_blank";

    // Construct the URI
    link.href = presentation;
    document.body.appendChild(link);
    link.click();

    // Cleanup the DOM
    document.body.removeChild(link);
    
    /*if(!presentation){
      Swal.fire(
        'Evento sin presentación',
        'You clicked the button!',
        'success'
      )

      return;
    }*/
  }

  getMySchedule() {
    this.serviceSchedule.getMySchedule(this.user.token)
      .subscribe(
        (res: Schedule[]) => {
          this.mySchedules = res;
          this.mySchedulesSearch = res;
          setTimeout(() => {
            Swal.close();
          }, 2000);
        }
      );
  }

  searchAgenda(text: string) {
    if (text.toString().length) {
      let even: any

      this.mySchedulesSearch.forEach(
        (agenda, index) => {
          even = agenda.events.filter(
            (evento) => {
              if (this.lang === 'es') {
                return evento.title.toLowerCase().indexOf(text.toLowerCase()) > -1;
              } else {
                return evento.title_en.toLowerCase().indexOf(text.toLowerCase()) > -1;
              }

            }
          );

          if (even) {
            this.myAgenda = {
              id: this.mySchedulesSearch[index].id,
              date: this.mySchedulesSearch[index].date,
              date_string: this.mySchedulesSearch[index].date_string,
              date_string_en: this.mySchedulesSearch[index].date_string_en,
              date_string_large: this.mySchedulesSearch[index].date_string_large,
              date_string_large_en: this.mySchedulesSearch[index].date_string_large_en,
              events: even
            };
            this.agendasearch.push(this.myAgenda);
          }
        }
      );
      this.mySchedules = this.agendasearch;
      this.agendasearch = [];
      return;
    }
    this.getMySchedule();
    Swal.close();
  }

  goMyAgenda(calendar) {
    // console.log(calendar, this.myAgendaCalendar);

    // if (calendar === 'office365' && this.myAgendaCalendar.length > 1) {
    //   this.openDialog({
    //     // image: 'assets/icons/hecho.svg',
    //     title: '¡Lo sentimos..!',
    //     // text: 'Por el momento solo puede agendar un evento a su calendario',
    //     text: 'De momento solo puede agendar un evento a la vez a su calendario',
    //     // optionalText: '¡Gracias por su participación!',
    //     btnAccept: '',
    //     btnCancel: 'Aceptar',
    //   });
    //   // setTimeout(() => {
    //   Swal.close();
    //   // }, 2000);
    //   return;
    // }

    const formData = new FormData();

    const language = this.lang === 'es' ? '1' : '2';

    formData.append('user_id', this.user.id);

    formData.append('language', language);

    this.myScheduleCalendar.forEach((event, index) => {
      formData.append(`event[${index}]`, event);
    });

    this.serviceSchedule.postEvents(formData).subscribe(
      (res: any) => {
        if (res.status === 200) {
          // this.openDialog({
          //   image: 'assets/icons/hecho.svg',
          //   title: '¡Registro exitoso!',
          //   text: 'Usted se ha registrado correctamente',
          //   // optionalText: '¡Gracias por su participación!',
          //   btnAccept: 'Aceptar',
          //   btnCancel: '',
          // });
          this.cleanScheduleCalendar();
          this.getMySchedule();
          this.getSchedule(this.user.token);
          this.goToCalendar(calendar);

        } else {
          this.openDialog({
            // image: 'assets/icons/hecho.svg',
            title: '¡Lo sentimos..!',
            text: res.message ? res.message : 'Ha ocurrido un error',
            // optionalText: '¡Gracias por su participación!',
            btnAccept: '',
            btnCancel: 'Aceptar',
          });
          setTimeout(() => {
            Swal.close();
          }, 2000);
        }
        // console.log(res);
      },
      (err) => {
        this.openDialog({
          // image: 'assets/icons/hecho.svg',
          title: '¡Lo sentimos..!',
          text: 'Ha ocurrido un error',
          // optionalText: '¡Gracias por su participación!',
          btnAccept: '',
          btnCancel: 'Aceptar',
        });
        console.error(err);
        setTimeout(() => {
          Swal.close();
        }, 2000);
      }
    );
  }

  goToCalendar(calendar: string) {
    this.loading();

    sessionStorage.setItem('_CALENDAR', calendar);

    this.myAgendaCalendar.forEach((e, i) => {
      const hourPE = moment(e.date).tz("America/Lima").format('HH:mm') + ' PE';
      const hourNY = moment(e.date).tz("America/New_York").format('HH:mm') + ' NY';
      const hourCL = moment(e.date).tz("America/Santiago").format('HH:mm') + ' CL';
      const hourStart = moment(e.date).format('DD-MM-YYYY HH:mm');
      const hourEnd = moment(e.date).add('75', 'minute').format('DD-MM-YYYY HH:mm');

      const description = `<p>${this.lang === 'es' ? 'Evento: ' + e.title : 'Event: ' + e.title_en}</p><br><p>${this.lang === 'es' ? 'Día: ' + e.date_string_large : 'Day: ' + e.date_string_large_en}</p><br><p>${this.lang === 'es' ? 'Hora: ' : 'Hour: '} ${hourPE} | ${hourNY} | ${hourCL}</p><br><p><a href="https://www.credicorpcapitalconference.com/event-live" target="_blank">${this.lang === 'es' ? 'Link de Acceso:' : 'Access Link:'} https://www.credicorpcapitalconference.com/event-live </a></p>`;

      const url = `https://www.addevent.com/dir/?client=${this.addEvent_ClientID}&start=${hourStart}&end=${hourEnd}&title=${this.lang === 'es' ? e.title : e.title_en}&timezone=America/Lima&service=${calendar}&description=${description}`;
      setTimeout(() => {
        Swal.close();
        window.open(url, '_blank');
      }, 2000);

    });

    this.myAgendaCalendar = [];

  }

  viewDetailSpeaker(idSpeaker: number) {
    this.serviceSchedule.getSpeaker(idSpeaker).subscribe(
      (res: any) => {
        // console.log(res);
        const dialogRef = this.dialog.open(ViewdetailsComponent, {
          // width: '250px',
          // height: '90vh',
          disableClose: true,
          data: {
            lang: this.lang,
            info: res,
            agenda: res.EventsbySpeaker,
          }
        });
      }
    );
  }

  viewDetailModerator(idModerator: number) {
    this.serviceSchedule.getModerator(idModerator).subscribe(
      (res: any) => {
        // console.log(res);
        const dialogRef = this.dialog.open(ViewdetailsComponent, {
          // width: '250px',
          // height: '90vh',
          disableClose: true,
          data: {
            lang: this.lang,
            info: res,
            agenda: res.EventsbyModerator,
          }
        });
      }
    );
  }

  viewDetailEvent(idEvent: number) {
    this.serviceSchedule.getEvent(idEvent).subscribe(
      (res: any) => {
        // console.log(res);
        const dialogRef = this.dialog.open(ViewdetaileventComponent, {
          // width: '250px',
          // height: '90vh',
          disableClose: true,
          data: {
            lang: this.lang,
            info: res,
            agenda: res.EventsbyModerator,
          }
        });
      }
    );
  }

  goToPage(page: string) {
    // console.log(page);
    this.route.navigateByUrl(page);
  }

  viewInstructions() {
    this.serviceConfig.getConfig()
      .subscribe(
        (res: any) => {
          const dialogRef = this.dialog.open(ViewinstructionsComponent, {
            // width: '25vw',
            // height: '45vh',
            disableClose: true,
            data: {
              lang: this.lang,
              info: res.txt_instructions
            }
          });
        }
      );
  }

}
