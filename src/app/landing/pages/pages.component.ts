import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  user: any;
  pathUrl: string;

  constructor(
    private location: Location,
  ) { }

  ngOnInit(): void {
  }

  // ngOnChanges(changes: SimpleChanges): void {
  //   this.user = localStorage.getItem('USER-DATA') ? true : false;
  //   this.pathUrl = this.location.path();
  //   console.log(this.user, this.pathUrl);
  //   // Llamado antes de cualquier otro gancho de ciclo de vida. Úselo para inyectar dependencias, pero evitar cualquier trabajo serio aquí.
  //   // agregar '$ {implementa onchanges}' a la clase.
  // }

  // ngOnDestroy(): void {
  //   console.log('destroy');
  // }

  // ngAfterViewChecked() {
  //   console.log('ngAfterViewChecked');
  // }

  // ngAfterContentChecked() {
  //   console.log('ngAfterContentChecked');
  // }



  ngDoCheck() {
    this.user = localStorage.getItem('USER-DATA') ? true : false;
    this.pathUrl = this.location.path();
  }


}
