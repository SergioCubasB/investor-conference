import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CredicorpComponent } from './credicorp.component';

describe('CredicorpComponent', () => {
  let component: CredicorpComponent;
  let fixture: ComponentFixture<CredicorpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CredicorpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CredicorpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
