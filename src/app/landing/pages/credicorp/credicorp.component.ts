import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { SharedService } from '../../../services/shared.service';
import { LanguageService } from '../../../services/language.service';
import { CredicorpService } from './services/credicorp.service';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-credicorp',
  templateUrl: './credicorp.component.html',
  styleUrls: ['./credicorp.component.scss']
})
export class CredicorpComponent implements OnInit {
  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 600,
    navText: ['<img src="../../../../assets/icons/left.svg" />', '<img src="../../../../assets/icons/rigth.svg">'],
    responsive: {
      0: {
        items: 2 
      },
      400: {
        items: 4
      },
      760: {
        items: 4
      },
      1000: {
        items: 4
      }
    },
    nav: true
  }
  credicorp: any;
  lang = 'es';

  awards = [];
  data: [] = [];
  dataExternal: [] = [];
  arrayTemporal = [];
  nuevoArray = [];


  constructor(
    private serviceLanguage: LanguageService,
    private navigate: Router,
    private serviceShared: SharedService,
    private credicorpService: CredicorpService,
    private sanitizer : DomSanitizer
  ) { }

  ngOnInit(): void {
    this.lang = sessionStorage.getItem('_LANG');
    //this.loading();
    this.switchLang();
    this.getInfoPage();
    this.slider(this.lang);
  }

  async loading() {
    Swal.fire({
      title: 'Cargando ...',
      allowEscapeKey: false,
      allowEnterKey: false,
      allowOutsideClick: false
    });
    await Swal.showLoading();
  }

  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  switchLang() {
    this.serviceLanguage.language.subscribe(
      (resp) => {
        this.lang = resp;
        this.slider(this.lang);        
      }
    );


    
    this.credicorpService.getCredicorp()
    .subscribe(
      (response: any) => {
        const  data  = response;
        this.data = data;
      }
    )

    this.credicorpService.getAttribute(1)
    .subscribe(
      (response: any) => {
        for(var i=0; i<response.length; i++){
          this.arrayTemporal = this.nuevoArray.filter(resp => resp["atr"] == response[i]["type_attribute_id"])
          if(this.arrayTemporal.length>0){
              this.nuevoArray[this.nuevoArray.indexOf(this.arrayTemporal[0])]["Sliders"].push( { image : 'http://159.223.201.87/'+response[i]["file"].route})
          }else{
              this.nuevoArray.push({"atr" : response[i]["type_attribute_id"], "title" : response[i]["atr"] , "Sliders" : [ { image : 'http://159.223.201.87/'+response[i]["file"].route}]})
          }
        }

      }
    )

    this.credicorpService.getAttributeExternal(1)
    .subscribe(
      (response: any) => {
        const  data  = response;
        this.dataExternal = data;
      }
    )

    
  }

  goToPage(url: string) {
    window.open(url, '_blank');
  }

  getInfoPage() {
    this.serviceShared.getCredicorp().subscribe(
      (res: any) => {
        this.credicorp = res;
        Swal.close();
        // console.log('this.credicorp :>> ', res);
      }
    );
  }

  slider(lang) {
    if (lang === 'es') {
      this.awards = [
        { path: 'assets/img/awards/es/01.png' },
        { path: 'assets/img/awards/es/02.png' },
        { path: 'assets/img/awards/es/03.png' },
        { path: 'assets/img/awards/es/04.png' },
        { path: 'assets/img/awards/es/05.png' },
        { path: 'assets/img/awards/es/06.png' },
      ];
    } else {
      this.awards = [
        { path: 'assets/img/awards/en/01.png' },
        { path: 'assets/img/awards/en/02.png' },
        { path: 'assets/img/awards/en/03.png' },
        { path: 'assets/img/awards/en/04.png' },
        { path: 'assets/img/awards/en/05.png' },
        // { path: 'assets/img/awards/en/06.png' },
      ];
    }
  }

}
