import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})
export class CredicorpService {
    protected api: string;

    constructor(
        private http: HttpClient
    ){
        this.api = `${environment.api}`;
    }

    getCredicorp(){
        return this.http.get(this.api + '/module/1/detail');
    }

    getPageEvent(){
        return this.http.get(this.api + '/module/2/detail');
    }

    getSocial(){
        return this.http.get(this.api + '/social-network');
    }
    
    getDate(){
        return this.http.get(this.api + '/event-date');
    }

    getAttributeExternal(id){
        return this.http.get(this.api + '/module/'+id+'/attribute-external');
    }

    getAttribute(id){
        return this.http.get(this.api + '/module/'+id+'/attribute?expand=atr');
    }

}