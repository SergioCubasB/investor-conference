import { DomSanitizer } from '@angular/platform-browser';
import { CredicorpService } from './../credicorp/services/credicorp.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { environment } from '../../../../environments/environment';
import { LanguageService } from '../../../services/language.service';
import { SharedService } from '../../../services/shared.service';
import { ScheduleService } from '../../../services/schedule.service';
import { MatDialog } from '@angular/material/dialog';
import { ViewdetailsComponent } from '../../../shared/components/viewdetails/viewdetails.component';

@Component({
  selector: 'app-speaker',
  templateUrl: './speaker.component.html',
  styleUrls: ['./speaker.component.scss']
})
export class SpeakerComponent implements OnInit {

  protected url = environment.api;
  lang = 'es';
  text: any;
  speakers: any;
  moderators: any;
  credicorp: any;
  dataExternal: [] = [];
  
  date_number:number = 29;
  date_mouth = 'JUL';
  constructor(
    private serviceLanguage: LanguageService,
    private service: ScheduleService,
    private serviceConfig: SharedService,
    public dialog: MatDialog,
    private credicorpService: CredicorpService,
    private sanitizer : DomSanitizer
  ) { }

  ngOnInit(): void {
    this.lang = sessionStorage.getItem('_LANG');
    this.getConfig();
    this.getSpeaker();
    this.getModerator();
    this.switchLang();
    this.loading();
  }

  async loading() {
    Swal.fire({
      title: 'Cargando ...',
      allowEscapeKey: false,
      allowEnterKey: false,
      allowOutsideClick: false
    });
    await Swal.showLoading();
  }

  
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  switchLang() {
    this.serviceLanguage.language.subscribe(
      (resp) => {
        this.lang = resp;
      }
    );

    this.credicorpService.getDate()
      .subscribe(
        (response: any) => {
          const  data = response;
          this.formatDate(data);
        }
      )

    this.credicorpService.getAttributeExternal(2)
        .subscribe(
          (response: any) => {
            const  data  = response;
            this.dataExternal = data;
          }
    )
  }

  getConfig() {
    this.serviceConfig.getConfig()
      .subscribe(
        (res: any) => {
          this.text = res.txt_agenda;
        }
      );
  }

  getSpeaker() {
    this.service.getSpeakerModerator(1).subscribe(
      (res: any) => {
        this.speakers = res;
        Swal.close();
      }
    );
  }

  getModerator() {
    this.service.getSpeakerModerator(2).subscribe(
      (res: any) => {
        this.moderators = res;
        // console.log('res moderators:>> ', res);
        Swal.close();
      }
    );
  }

  viewDetailSpeaker(idSpeaker: number) {
    this.service.getSpeaker(idSpeaker).subscribe(
      (res: any) => {
        // console.log(res);
        const dialogRef = this.dialog.open(ViewdetailsComponent, {
          // width: '250px',
          // height: '90vh',
          disableClose: true,
          data: {
            lang: this.lang,
            info: res,
            agenda: res.EventsbySpeaker,
          }
        });
      }
    );
  }

  viewDetailModerator(idModerator: number) {
    this.service.getModerator(idModerator).subscribe(
      (res: any) => {
        const dialogRef = this.dialog.open(ViewdetailsComponent, {
          // width: '250px',
          // height: '90vh',
          disableClose: true,
          data: {
            lang: this.lang,
            info: res,
            agenda: res.EventsbyModerator,
          }
        });
      }
    );
  }

  goToLink(file: string) {
    window.open(`assets/files/${file}.pdf`, '_blank');
  }
  
  formatDate(date: any){
    const dateFormat = new Date(date[0].start_date);

    const day = date[0].start_date.slice(-2);
    const mouth = dateFormat.toLocaleString('default', { month: 'long' }).slice(0,3);

    this.date_number =  day;
    this.date_mouth = mouth;
  }

}
