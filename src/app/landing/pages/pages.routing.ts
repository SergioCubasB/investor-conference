import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ScheduleComponent } from './schedule/schedule.component';
import { CredicorpComponent } from './credicorp/credicorp.component';
import { EventComponent } from './event/event.component';
import { SpeakerComponent } from './speaker/speaker.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'credicorp',
    pathMatch: 'full'
  },
  {
    path: 'credicorp',
    component: CredicorpComponent
  },
  {
    path: 'schedule',
    component: ScheduleComponent
  },
  {
    path: 'speaker',
    component: SpeakerComponent
  },
  {
    path: 'event',
    component: EventComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }
